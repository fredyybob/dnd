import discord
from discord.ext import commands

import main
from main import addArray, removeArray
from random import randint
import downtime
import config
from functools import wraps

client = commands.Bot(command_prefix="|")
database = downtime.DatabaseConnection(config.downtime_file)


def character_call_decorator(func):
    @wraps(func)
    async def wrapper_error_handling(*args, **kwargs):
        ctx = args[0]
        try:
            await func(*args, **kwargs)
        except discord.ext.commands.errors.CommandNotFound:
            await ctx.send
        except KeyError:
            await ctx.send('please enter an existing valid character name')
        except TypeError:
            await ctx.send('please enter the appropriate value types')

    return wrapper_error_handling


@client.event
async def on_ready():
    # this function prints ready status into python terminal
    print("bot is ready")


@client.command()
async def hello(ctx):
    # This function provides a facisimile of human interaction for the emotionally deprived
    await ctx.send('Hi')


@client.command(aliases=["show", "show_stats", "show_it_to_me_daddy"])
async def show_me_the_goods(ctx):
    # this function prints the csv into the chat and adds row numbers
    scores = main.give_arrays('stat arrays.csv')
    await ctx.send(scores)


@client.command(aliases=["pick", "pick_stat", "pick_stat_array"])
async def give_me_the_goods(ctx, *, number):
    # this function allows the user to pick a stat array from the csv, removes the stat array from the csv and rolls a new one to replace it.

    try:
        if int(number) <= 0:
            raise ValueError
        await ctx.send(str(ctx.author) + "has claimed " + str(removeArray('stat arrays.csv', number)))
        await ctx.send(str(ctx.author) + "has added " + str(addArray('stat arrays.csv')))
    except discord.errors.HTTPException:
        await ctx.send('there are no goods')
    except (ValueError, TypeError):
        await ctx.send('You input an invalid line number')

    # this function will give remove a stat array from the file and print the chosen array and command users name
    # this will also roll another array and add it to the end


@client.command()
async def roll_hp(ctx, *, number):
    # this function will publicly roll a die for rolled hp level ups
    # will catch error if user inputs a non number or too small of a number
    try:
        if int(number) <= 0:
            raise ValueError
        await ctx.send(randint(1, number))
    except ValueError:
        await ctx.send('Please input a reasonable number for your hit die size')
    except TypeError:
        await ctx.send('Please input a number for the size of the hitdie')


@client.command(aliases=['mr'])
async def mroll(ctx, numrolls, dc, bonus):
    try:
        results = main.multiroll(int(numrolls), int(dc), int(bonus))
        await ctx.send(
            f"Rolling {numrolls} times with a dc of {dc} and a bonus of {bonus} {database.json_data[str(ctx.author.id)]['character_name']} succeeded {results[0]} times and failed {results[1]} times")
    except ValueError:
        await ctx.send("did you enter integers?")
    except:
        await ctx.send("there was an issue.  please wait while we resolve")


@client.command(aliases=['r'])
async def roll(ctx, die_size, bonus=0):
    try:
        result = randint(1, int(die_size)) + bonus
        await ctx.send(f'{database.json_data[str(ctx.author.id)]["character_name"]} rolled a {result}')
    except ValueError:
        await ctx.send("did you enter integers?")
    except:
        await ctx.send("there was an issue please wait while we resolve")

@client.command()
async def help_me(ctx):
    # Prints a help function that lists commands
    await ctx.send(
        "|show_stats - Gives all stats available\n|pick_stat (line number here) - Removes selected scores and rolls a "
        "new one\n|roll_hp (hit die here)- rolls a die of given size 6 for d6 etc")


@client.command(aliases=['retire'])
async def delete_character(ctx):
    # removes a character from ledger
    try:
        character_name = database.json_data[str(ctx.author.id)]['character_name']
        del database.json_data[str(ctx.author.id)]
        await ctx.send(character_name + " has retired and/or died.")
    except KeyError:
        await ctx.send('please enter a valid character name')


@client.command()
async def create_character(ctx, *, character_name):
    # creates a new character
    try:
        downtime.new_char(database, ctx.author.id, character_name)
        await ctx.send(f'welcome to farcrystal {character_name}!')
    except downtime.CharacterAlreadyExistsError:
        await ctx.send('this name is taken, please use a different name or use |delete_character')


@client.command()
@character_call_decorator
async def check_downtime(ctx):
    # retrieves downtime days for character
    character_info = downtime.update_downtime(database, ctx.author.id)
    days = character_info["downtime_days"]
    await ctx.send(f'you have {days} days of downtime')


@client.command()
@character_call_decorator
async def use_downtime(ctx, days, task):
    # changes the days of downtime by the given value
    character_info = downtime.update_downtime(database, ctx.author.id, -1 * int(days))
    await ctx.send(f'{character_info["character_name"]} has spent {days} days hard at work {task}')
    await ctx.send(f'you have {character_info["downtime_days"]} days of downtime')


# maybe have a waiting queue if too many requests? this is a request from ilyas that I am putting in a queue thus
# making it ironic add in lots of try catch shit

client.run(config.botkey)
