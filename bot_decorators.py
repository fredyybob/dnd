
def character_call_decorator(func):
    def wrapper_error_handling():
        try:
            func
        except KeyError:
            await ctx.send('please enter a valid input')
