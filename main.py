# This project is to generate stat arrays for dnd 5e

import csv
from random import randint
import statistics
from math import floor
import shutil
from tempfile import NamedTemporaryFile


def fourd6droplow():
    d6rolls = []
    for x in range(0, 4):
        d6rolls.append(randint(1, 6))
    d6rolls.remove(min(d6rolls))
    return sum(d6rolls)


def roll(die_size, dc, bonus):
    return randint(1, die_size) + bonus >= dc


def multiroll(numrolls, dc, bonus):
    success = 0
    for i in range(numrolls):
        success += roll(20, dc, bonus)
    return success, numrolls - success


def getCSVLength(filename):
    file = open(filename)
    reader = csv.reader(file)
    return len(list(reader))


def genArray():
    while True:
        row = []
        for x in range(0, 6):
            row.append(fourd6droplow())
        if isValidArray(row):
            return row


def isValidArray(scores):
    # define rules for a valid stat array here
    if statistics.variance(scores) > 4:
        x = lambda a: floor(a / 2 - 5)
        if sum(map(x, scores)) > 2:
            return True
    return False


def removeArray(filename, lineNum):
    # remove a stat array from the csv

    temp_file = NamedTemporaryFile(delete=False, mode='w', newline='')
    removedLine = ''
    lineNum = int(lineNum)
    with open(filename, "r") as csvfile, temp_file:
        reader = csv.reader(csvfile)
        writer = csv.writer(temp_file)
        for i, row in enumerate(reader):
            if i != int(lineNum):
                mydummyRow = [i.strip() for i in row]
                writer.writerow(mydummyRow)
            else:
                removedLine = [i.strip() for i in row]

    shutil.move(temp_file.name, filename)
    return removedLine


def addArray(filename, statArray=None):
    # add a stat array to the csv
    if statArray is None:
        statArray = genArray()
    mydummystatArray = [str(i) for i in statArray]
    temp_file = NamedTemporaryFile(delete=False, mode='w', newline='')
    with open(filename, "r") as csvfile, temp_file:
        reader = csv.reader(csvfile)
        writer = csv.writer(temp_file)
        for row in reader:
            writer.writerow(row)
        writer.writerow(mydummystatArray)
    shutil.move(temp_file.name, filename)
    return mydummystatArray


def give_arrays(filename):
    with open(filename, 'r') as ifs:
        reader = ifs.readlines()
        dummy_scores = ''
        padding = 4

        for i, line in enumerate(reader):
            line = line.strip().split(',') + ['|', str(i) if i > 0 else 'Stat Array Number']
            dummy_scores += f"{''.join(' ' * (padding - len(a)) + a for a in line)}\n"

        scores = '```' + dummy_scores + '```'
        return scores


def main():
    pass
    #    with open("stat arrays.csv", 'w', newline='') as ofs:
    #     writer = csv.writer(ofs)
    #     writer.writerow(['Strength', 'Dexterity', 'Constitution', 'Intelligence', 'Wisdom', 'Charisma'])
    #     for y in range(0, 20):
    #         row = genArray()
    #         if statistics.variance(row) > 4:
    #             writer.writerow(row)


if __name__ == '__main__':
    main()
