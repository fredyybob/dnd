import json

import downtime
import pytest
import datetime

testing_database = downtime.DatabaseConnection("test_file")


def generate_test_file(author_ids, character_name, date, downtime_days):
    test_dict = {}
    for characters in zip(author_ids, character_name, date, downtime_days):
        test_dict[characters[0]] = {"character_name": characters[1],
                                    "last_updated": characters[2],
                                    "downtime_days": characters[3]
                                    }
        return json.loads(json.dumps(test_dict))



@pytest.mark.parametrize()
def test_character_deletion():
    author_ids = []
    downtime_days = []
    dates = []
    for i in range(1, 10):
        author_ids.append(i)
        downtime_days.append(i)
        dates.append(str(datetime.date.today()))
    character_names = ["frank", "bob", "c", "d", 'e', "frank", "bob", "c", "d", 'e']
    generate_test_file(author_ids, character_names, dates, downtime_days)
    with pytest.raises(KeyError):
        downtime.delete_char('test_file', 11)

"""
write tuples with test data + expected error

"""