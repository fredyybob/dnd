"""
    downtime module:
        Function to create a character to begin accruing downtime
        function to check how much downtime has been accrued
        function to consume downtime

        check downtime accumulation every request

json file player data to
"""
import json
import datetime
import shutil
from tempfile import NamedTemporaryFile
import os

"""
x = {
    character_name : {
    last_updated: datetime,
    downtime_days: intofdays
    }
}

should open the json somewhere else and execute the transform on the 
"""


class Error(Exception):
    "base class for other exceptions"
    pass


class CharacterAlreadyExistsError(Error):
    "raise when attempting to make another character that already exists"
    pass


class DatabaseConnection:

    def __init__(self, filename):
        self.filename = filename
        self.json_data = {}
        if os.path.isfile(self.filename):
            with open(self.filename, 'r') as ifs:
                self.json_data = json.load(ifs)




    def __enter__(self):
        return self.json_data

    def __exit__(self, exc_type, exc_val, exc_tb):
        self.save()

    def save(self):
        with NamedTemporaryFile(delete=False, mode='w', newline='') as temp_file:
            json.dump(self.json_data, temp_file)
            temp_name = temp_file.name
        shutil.move(temp_name, self.filename)


def new_char(database, author_id, character_name) -> dict:
    # adds a new character to the file
    author_id = str(author_id)

    character_info = {}

    with database as db:
        # write exception handling here?
        if author_id in db:
            raise CharacterAlreadyExistsError
        # author_id is an int will be converted to a string by json
        db[author_id] = {"character_name": character_name,
                         "last_updated": str(datetime.date.today()),
                         "downtime_days": 0
                         }
        character_info = db[author_id]

    return character_info


def update_downtime(database, author_id, days_change=0) -> dict:
    # adjusts downtime days by a given int
    author_id = str(author_id)

    with database as db:
        delta = datetime.datetime.now() - datetime.datetime.strptime(db[author_id]["last_updated"], '%Y-%m-%d')
        if delta.days >= 1:
            db[author_id]['downtime_days'] += int(delta.days)
            db[author_id]['last_updated'] = str(datetime.date.today())
        db[author_id]['downtime_days'] += int(days_change)
        character_info = db[author_id]

    return character_info
